# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table contact (
  id                        bigint auto_increment not null,
  first_name                varchar(255),
  last_name                 varchar(255),
  constraint pk_contact primary key (id))
;

create table user (
  login                     varchar(255) not null,
  email                     varchar(255),
  password                  varchar(255),
  constraint pk_user primary key (login))
;




# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table contact;

drop table user;

SET FOREIGN_KEY_CHECKS=1;

