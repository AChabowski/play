package models;

import controllers.Secured;
import play.db.ebean.Model;
import play.mvc.Result;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static controllers.Contacts.contacts;
import static play.data.Form.form;
import static play.mvc.Results.forbidden;
import static play.mvc.Results.ok;


@Entity
public class Contact extends Model {

    @Id
    public Long id;
    public String firstName;
    public String lastName;



    public List<User> members = new ArrayList<User>();

    public Contact(String firstName, String lastName,User owner ){
        this.firstName = firstName;
        this.lastName = lastName;
        this.members.add(owner);
    }

    public static Model.Finder<Long,Contact> find = new Model.Finder(Long.class,Contact.class);



    public static List<Contact> findInvolving (String user){
        return find.where()
                .eq("members.email", user)
                .findList();
    }

    public static List<Contact> findAll(){
        return find.all();
    }


    public static boolean remove(Contact contact) {
        return contacts.remove(contact);
    }

}
