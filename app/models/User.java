package models;

import oauth.signpost.http.HttpResponse;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class User extends Model {

    @Id
    public String login;
    public String email;
    public String password;


    public static Model.Finder<String,User> find = new Model.Finder(String.class,User.class);

    public User(String login, String email, String password) {
        this.login = login;
        this.email = email;
        this.password = password;
    }



    public static void save (User user){
        user.save();
    }

    public static User authenticate(String login,String password){
        return find.where()
                .eq("login",login)
                .eq("password",password)
                .findUnique();
    }
}
