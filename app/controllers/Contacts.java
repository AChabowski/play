package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Transaction;
import models.Contact;
import models.User;
import play.data.Form;
import play.mvc.*;
import views.html.contacts.item;
import views.html.home;

import javax.persistence.PersistenceException;
import java.util.List;





@Security.Authenticated(Secured.class)
public class Contacts extends Controller {


    public static Form<Contact> contactForm = Form.form(Contact.class);
    public static List<Contact> contacts = Contact.findAll();


    public static Result addContact(){
        List<Contact> contacts = Contact.findAll();
        return ok(item.render(contactForm,contacts));
    }

    public static Result saveContact(String user){
        Form<Contact> boundForm = contactForm.bindFromRequest();
        if (boundForm.hasErrors()){
            flash("error","Please correct the form below");
            return badRequest(item.render(boundForm,contacts));
        } else {
            Contact contact = boundForm.get();
            contact.save();
            flash("success", String.format("Successfully added contact", contact ));
        }
        return redirect(routes.Contacts.addContact());
    }

    public static Result delete(Long id) {
        Contact.find.ref(id).delete();
        return redirect(routes.Contacts.addContact());
    }

    public static Result edit (Long id){
        Form<Contact> contactForm = Form.form(Contact.class).fill(
                Contact.find.byId(id));
        return ok(views.html.contacts.editForm.render(id,contactForm));
    }

    public static Result update(Long id) throws PersistenceException{
        Form<Contact> contactForm = Form.form(Contact.class).bindFromRequest();
        if(contactForm.hasErrors()){
            return badRequest(views.html.contacts.editForm.render(id,contactForm));
        }
        Transaction txn = Ebean.beginTransaction();
        try{
            Contact saveContact = Contact.find.byId(id);
            System.out.print(saveContact);
            if(saveContact != null){
                Contact newContactData = contactForm.get();
                saveContact.firstName = newContactData.firstName;
                saveContact.firstName = newContactData.lastName;

                saveContact.update();
                flash("success", "Contact " + contactForm.get().firstName + " has been update");
                txn.commit();
            }
        } finally {
            txn.end();
        }
        return redirect(routes.Contacts.addContact());
    }


}


