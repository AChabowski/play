package controllers;

import models.User;
import models.Contact;
import play.data.*;
import play.mvc.*;
import views.html.*;
import static play.data.Form.form;
import static play.mvc.Security.*;


public class Application extends Controller {


    public static Result index() {

        return ok(index.render("Your new application is ready.",form(Login.class)));
    }


    public static class Login {

        public String login;
        public String password;

        public String validate(){
            if (User.authenticate(login,password) == null){
                return "Invalid user or password";
            }
            return null;
        }
    }

    public static Result authenticate (){
        Form<Login> loginForm = form(Login.class).bindFromRequest();
        if (loginForm.hasErrors()){
            return badRequest(index.render("tutu",loginForm));
        } else {
            session().clear();
            session("login",loginForm.get().login);
            return redirect(routes.Application.dashboard());
        }
    }

    @Authenticated(Secured.class)
    public static Result dashboard(){
        return ok(home.render(User.find.byId(request().username()
                )));
    }

    public static Result logout(){
        session().clear();
        flash("success","You've been logged out");
        return redirect(routes.Application.index());
    }
}
