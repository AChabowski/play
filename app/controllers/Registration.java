package controllers;

import models.User;
import play.data.Form;
import play.data.validation.Constraints;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.main;
import views.html.register;

import javax.persistence.criteria.From;

import static play.data.Form.form;


public class Registration extends Controller {

    public static Form<User> userForm = Form.form(User.class);

    public static Result register (){
        return ok(register.render(userForm));
    }


    public static class SaveUser{
        @Constraints.Required
        public String login;
        @Constraints.Required
        public String email;
        @Constraints.Required
        public String password;
    }



    public static Result saveUser(){
    Form<User> filledForm = userForm.bindFromRequest();
        if(filledForm.hasErrors()){
            flash("error", "Sorry, something went wrong. Please try again.");
            return redirect(routes.Application.index());
        } else {
            User.save(filledForm.get());
            session("connected", filledForm.data().get("login"));
            flash("success", ("User was successfully created."));
            return redirect (routes.Application.index());

        }
    }


}
